﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;

namespace Davydova_Task4_Library.BLL.Implementations
{
    public class BookLogic : IBookLogic
    {
        private IBookDao _bookDao;
        private IBaseDao _baseDao;
        private IAuthorDao _authorDao;

        public BookLogic(IBookDao bookDao, IBaseDao baseDao, IAuthorDao authorDao)
        {
            _bookDao = bookDao;
            _baseDao = baseDao;
            _authorDao = authorDao;
        }
        public int AddBook(int id, string name, string countOfPages, string note, string placeOfPublication, string publishingHouse, string publishingYear, List<Author> authors, string iSBN)
        {
            BookValidation bookValidation = new BookValidation();
            PublicationValidation publicationValidation = new PublicationValidation();
            LibraryObjectValidation libraryObjectValidation = new LibraryObjectValidation();
            bookValidation.ExecuteBookValidation(iSBN);
            publicationValidation.ExecutePublicationValidation(publishingHouse, publishingYear, placeOfPublication);
            libraryObjectValidation.ExecuteLibraryObjectValidation(name, countOfPages, note);
            if (!bookValidation.exceptionsList.Any() && !publicationValidation.exceptionsList.Any() && !libraryObjectValidation.exceptionsList.Any())
            {
                foreach(Book bookItem in _bookDao.GetAllBooks())
                {
<<<<<<< Updated upstream
                    if(bookItem.ISBN == iSBN)
=======
                    foreach (Book bookItem in _bookDao.GetAllBooks())
                    {
                        if (bookItem.ISBN == iSBN)//add checking unique for sequence atribute
                        {
                            throw new ArgumentException();
                        }
                    }
                    Book book = new Book(id, name, int.Parse(publishingYear), int.Parse(countOfPages), note, placeOfPublication, publishingHouse, authors, iSBN);
                    foreach (Author author in authors)
>>>>>>> Stashed changes
                    {
                        throw new ArgumentException("ISBN must be unique.");
                    }
                }
                Book book = new Book(id, name, int.Parse(publishingYear), int.Parse(countOfPages), note, placeOfPublication, publishingHouse, authors, iSBN);
                return _bookDao.AddBook(book);
            }
            else
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in bookValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                throw new ValidationException(failedValidation.ToString());
            }
        }

        public bool DeleteBook(int id)
        {
            return _bookDao.DeleteBook(id);
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return _bookDao.GetAllBooks();
        }
        
        public IEnumerable<Book> GetBooksByIdAuthor(int id)
        {
            return _baseDao.GetBooksByAuthor(_authorDao.GetAuthorById(id));
        }

        public IEnumerable<Book> GetBooksByName(string name)
        {
            return (IEnumerable<Book>)_baseDao.GetLibraryObjectByName(name);
        }

        public IEnumerable<Book> GetBooksByNameAuthor(string name)
        {
            return (IEnumerable<Book>)_baseDao.GetBooksByAuthor(_authorDao.GetAuthorByName(name));
        }

        public void GetReverseSortedBookByPublicationYear()
        {
            _baseDao.ReverseSortByPublicationYear();
        }

        public void GetSortedBookByPublicationYear()
        {
            _baseDao.SortByPublicationYear();
        }

        public IEnumerable<IEnumerable<Book>> GroupByPublicationYear()
        {
            return (IEnumerable<IEnumerable<Book>>)_baseDao.GroupByPublicationYear();
        }

        IEnumerable<Book> IBookLogic.GetBooksByCharacterSet(string characterSet)
        {
            return (IEnumerable<Book>)_baseDao.GetBooksStartByChars(characterSet);
        }
    }
}
