﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL
{
    public class PublicationLogic : IPublicationLogic
    {
        private IPublicationDao _publicationDao;

        public PublicationLogic(IPublicationDao publicationDao)
        {
            _publicationDao = publicationDao;
        }
        public int AddPublication(int id, string name, string publicationYear, string countOfPages, string note, string placeOfPublication, string publishingHouse)
        {
            PublicationValidation publicationValidation = new PublicationValidation();
            LibraryObjectValidation libraryObjectValidation = new LibraryObjectValidation();
            libraryObjectValidation.ExecuteLibraryObjectValidation(name, countOfPages, note);
            publicationValidation.ExecutePublicationValidation(publishingHouse, publicationYear, placeOfPublication);
            if (!publicationValidation.exceptionsList.Any() && !libraryObjectValidation.exceptionsList.Any())
            {
                Publication publication = new Publication(id, name, int.Parse(publicationYear), int.Parse(countOfPages), note, placeOfPublication, publishingHouse);
                return _publicationDao.AddPublication(publication);
            }
            else
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in publicationValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                throw new ValidationException(failedValidation.ToString());
            }
        }

        public bool DeletePublication(int publicationId)
        {
            return _publicationDao.DeletePublication(publicationId);
        }

        public IEnumerable<Publication> GetAllPublication()
        {
            return _publicationDao.GetAllPublications();
        }

        public Publication GetPublicationById(int idPublication)
        {
            return _publicationDao.GetPublicationById(idPublication);
        }
    }
}
