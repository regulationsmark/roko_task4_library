﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.Publications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class BookDao:IBookDao
    {
        List<Book> books = new List<Book>();
        public Book GetBookById(int bookId)
        {
            foreach (Book book in books)
            {
                if (book.Id == bookId)
                {
                    return book;
                }
            }
            return null;
        }
            

        public int AddBook(Book book)
        {
            books.Add(book);
            return books.Last().Id;
        }

        public bool DeleteBook(int bookId)
        {
            foreach (Book book in books)
            {
                if (book.Id == bookId)
                {
                    return books.Remove(book);
                }
            }
            return false;
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return books;
        }

        public void UpdateBook(Book book)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Book> GetBooksByAuthorId(int authorId)
        {
            throw new NotImplementedException();
        }
    }
}
