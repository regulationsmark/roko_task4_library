﻿using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class PatentDao : IPatentDao
    {
        List<Patent> patents = new List<Patent>();
        public int AddPatent(Patent patent)
        {
            patents.Add(patent);
            return patents.Last().Id;
        }

        public bool DeletePatent(int patentId)
        {
            foreach (Patent patent in patents)
            {
                if (patent.Id == patentId)
                {
                    return patents.Remove(patent);
                }
            }
            return false;
        }

        public IEnumerable<Patent> GetAllPatents()
        {
            return patents;
        }

        public Patent GetPatentById(int idPatent)
        {
            foreach (Patent patent in patents)
            {
                if (patent.Id == idPatent)
                {
                    return patent;
                }
            }
            return null;
        }
    }
}
