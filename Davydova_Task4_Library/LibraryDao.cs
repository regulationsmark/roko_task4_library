﻿using Davydova_Task4_Library.DAL_Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class LibraryDao : ILibraryObject
    {
        List<LibraryObject> libraryObjects = new List<LibraryObject>();
        public int AddLibraryObject(LibraryObject libraryObject)
        {
            libraryObjects.Add(libraryObject);
            return libraryObjects.Last().Id;
        }

        public bool DeleteLibraryObject(int libraryObjectId)
        {
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject.Id == libraryObjectId)
                {
                    return libraryObjects.Remove(libraryObject);
                }
            }
            return false;
        }

        public IEnumerable<LibraryObject> GetAllLibraryObjects()
        {
            return libraryObjects;
        }

       

        

        public LibraryObject GetLibraryObjectById(int idLibraryObject)
        {
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject.Id == idLibraryObject)
                {
                    return libraryObject;
                }
            }
            return null;
        }

        public LibraryObject GetLibraryObjectByName(string name)
        {
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject.Name == name)
                {
                    return libraryObject;
                }
            }
            return null;
        }
    }
}
