﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;

namespace Davydova_Task4_Library.DAL
{
    public class BaseDao : IBaseDao
    {
        List<LibraryObject> libraryObjects = new List<LibraryObject>();

        public int AddLibraryObject(LibraryObject libraryObject)
        {
            libraryObjects.Add(libraryObject);
            return libraryObjects.Last().Id;
        }
        public bool DeleteLibraryObject(int idLibraryObject)
        {
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject.Id == idLibraryObject)
                {
                    return libraryObjects.Remove(libraryObject);
                }
            }
            return false;
        }
        public IEnumerable<LibraryObject> GetAllLibraryObjects()
        {
            return libraryObjects;
        }
        public IEnumerable<LibraryObject> GetLibraryObjectByName(string nameLibraryObject)
        {
            List<LibraryObject> libraryObjects = new List<LibraryObject>();
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject.Name == nameLibraryObject)
                {
                    libraryObjects.Add(libraryObject);
                }
            }
            return libraryObjects;
        }
        public IEnumerable<LibraryObject> SortByPublicationYear()
        {
<<<<<<< Updated upstream
            libraryObjects.Sort();
=======
            _libraryObjects.Sort();
            _logger.LogDebug($"{_libraryObjects.Count} libary objects was sorted.");
            return _libraryObjects;
>>>>>>> Stashed changes
        }
        public IEnumerable<LibraryObject> ReverseSortByPublicationYear()
        {
<<<<<<< Updated upstream
            libraryObjects.Sort();
            libraryObjects.Reverse();
=======
            _libraryObjects.Sort();
            _libraryObjects.Reverse();
            _logger.LogDebug($"{_libraryObjects.Count} libary objects was sorted in reverse order.");
            return _libraryObjects;
>>>>>>> Stashed changes
        }
        public IEnumerable<IEnumerable<LibraryObject>> GroupByPublicationYear()
        {
            return libraryObjects.GroupBy(libObj => libObj.PublicationYear);
        }

        public IEnumerable<Book> GetBooksByAuthorId(int authorId)
        {
            List<Book> books = new List<Book>();
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject is Book)
                {
                    foreach (Author author in ((Book)libraryObject).Authors)
                    {
                        if (author.Id == authorId)
                        {
                            books.Add((Book)libraryObject);
                        }
                    }
                }
            }
<<<<<<< Updated upstream
=======
            _logger.LogDebug($"{books.Count} books was recieved by author with id {authorId}.");
            _logger.LogDebug($"Recieved books { JsonConvert.SerializeObject(books)}.");
>>>>>>> Stashed changes
            return books;
        }

        public IEnumerable<Patent> GetPatentsByInventorId(int inventorId)
        {
            List<Patent> patents = new List<Patent>();
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject is Patent)
                {
                    foreach (Author inventor in ((Patent)libraryObject).Inventors)
                    {
                        if (inventor.Id == inventorId)
                        {
                            patents.Add((Patent)libraryObject);
                        }
                    }
                }
            }
<<<<<<< Updated upstream
=======
            _logger.LogDebug($"{patents.Count} patents was recieved by inentor with id {inventorId}.");
            _logger.LogDebug($"Recieved petents { JsonConvert.SerializeObject(patents)}.");
>>>>>>> Stashed changes
            return patents;
        }

        public IEnumerable<LibraryObject> GetBooksAndPatentsByAuthorId(int authorId)
        {
            List<LibraryObject> libraryObjects = new List<LibraryObject>();
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject is Book)
                {
                    foreach (Author author in ((Book)libraryObject).Authors)
                    {
                        if (author.Id == authorId)
                        {
                            libraryObjects.Add((Book)libraryObject);
                        }
                    }
                }
                if (libraryObject is Patent)
                {
                    foreach (Author inventor in ((Patent)libraryObject).Inventors)
                    {
                        if (inventor.Id == authorId)
                        {
                            libraryObjects.Add((Patent)libraryObject);
                        }
                    }
                }
            }
<<<<<<< Updated upstream
            return libraryObjects;
        }

        //add chars to string in BL
        IEnumerable<IEnumerable<Book>> IBaseDao.GetBooksStartByChars(string publishingHouse)
=======
            _logger.LogDebug($"{libraryObjects.Count} books and patents was recieved by inentor with id{authorId}.");
            _logger.LogDebug($"Recieved books and petents { JsonConvert.SerializeObject(libraryObjects)}.");
            return libraryObjects;
        }

        //groupby
        public IEnumerable<Book> GetBooksStartByChars(string publishingHouse)
>>>>>>> Stashed changes
        {
            List<Book> books = new List<Book>();
            foreach (LibraryObject libraryObject in libraryObjects)
            {
                if (libraryObject is Book)
                {
                    if (((Book)libraryObject).PublishingHouse.StartsWith(publishingHouse))
                    {
                        books.Add((Book)libraryObject);
                    }
                }
            }
<<<<<<< Updated upstream
            return books.GroupBy(books => books.PublishingHouse);
=======
            _logger.LogDebug($"{books.Count} books was group by publishing house {publishingHouse}. Ids: { JsonConvert.SerializeObject(books.Select(p => p.Id)) }");
            //return books.GroupBy(books => books.PublishingHouse);
            return books;
        }

        public void UpdateLibraryObject(LibraryObject libraryObject)
        {
            throw new NotImplementedException();
>>>>>>> Stashed changes
        }
    }
}
