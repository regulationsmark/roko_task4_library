﻿using Davydova_Task4_Library.Library.Publications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Interfaces
{
    public interface INewspaperLogic
    {
        int AddNewspaper(int id, string name, string publicationYear, string countOfPages, string note, string placeOfPublication, string publishingHouse, int issueNumber, DateTime issueDate, string iSSN);

        bool DeleteNewspaper(int newspaperId);

        NewsPaper GetNewspaperById(int newspaperId);

        IEnumerable<NewsPaper> GetAllNewsPapers();
    }
}
