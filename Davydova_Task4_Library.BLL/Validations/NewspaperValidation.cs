﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Validations
{
    public class NewspaperValidation
    {
        public List<string> exceptionsList = new List<string>();

        public bool ISSNValidation(string iSSN)
        {
            bool result = true;
            if (!string.IsNullOrWhiteSpace(iSSN))
            {
                var regex = new Regex(@"^ISSN [0-9]{4}-[0-9]{4}$");
                if (!regex.IsMatch(iSSN))
                {
                    result = false;
                    exceptionsList.Add("Incorrect ISSN.");
                }
            }
            return result;
        }

        public bool IssueDateValidation(DateTime? issueDate, int publicationYear)
        {
            bool result = true;
            if (issueDate != null)
            {
               if (issueDate.Value.Year != publicationYear)
               {
                    result = false;
                    exceptionsList.Add("Issue date and publication year must be equal.");
               }
            }
            else
            {
                result = false;
                exceptionsList.Add("Issue date cannot be empty.");
            }
            return result;
        }

        public bool ExecuteNewspaperValidation(string iSSN, DateTime? issueDate, int publicationYear)
        {
            return ISSNValidation(iSSN) && IssueDateValidation(issueDate, publicationYear);
        }
    }
}
