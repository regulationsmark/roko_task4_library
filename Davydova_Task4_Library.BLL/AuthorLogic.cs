﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL
{
    public class AuthorLogic:IAuthorLogic
    {
        private IAuthorDao _authorDao;

        public AuthorLogic(IAuthorDao authorDao)
        {
            _authorDao = authorDao;
        }

        public int AddAuthor(int id, string name, string surname)
        {
            AuthorValidation authorValidation = new AuthorValidation();
            authorValidation.ExecuteAuthorValidation(name, surname);
            if (!authorValidation.exceptionsList.Any())
            {
                Author author = new Author(id, name, surname);
                return _authorDao.AddAuthor(author);
            }
            else
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in authorValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                throw new ValidationException(failedValidation.ToString());
            }
        }
        public bool DeleteAuthor(int authorId)
        {
            return _authorDao.DeleteAuthor(authorId);
        }

        public IEnumerable<Author> GetAllAuthors()
        {
            return _authorDao.GetAllAuthors();
        }

        public Author GetAuthorById(int idAuthor)
        {
            return _authorDao.GetAuthorById(idAuthor);
        }

        public IEnumerable<Author> GetAuthorsByBookId(int bookId)
        {
            return _authorDao.GetAuthorsByBookId(bookId);
        }

        public Author GetAuthorByName(string nameAuthor)
        {
            return _authorDao.GetAuthorByName(nameAuthor);
        }

        public IEnumerable<Author> GetInventorsByPatentId(int patentId)
        {
            return _authorDao.GetInventorsByPatentId(patentId);
        }

        public void UpdateAuthor(Author author)
        {
            _authorDao.UpdateAuthor(author);
        }
    }
}
