﻿using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class NewspaperDao : INewspaperDao
    {
        List<NewsPaper> newsPapers = new List<NewsPaper>();
        public int AddNewsPaper(NewsPaper newspaper)
        {
            newsPapers.Add(newspaper);
            return newsPapers.Last().Id;
        }

        public bool DeleteNewsPaper(int newspaperId)
        {
            foreach (NewsPaper newsPaper in newsPapers)
            {
                if (newsPaper.Id == newspaperId)
                {
                    return newsPapers.Remove(newsPaper);
                }
            }
            return false;
        }

        public IEnumerable<NewsPaper> GetAllNewspapers()
        {
            return newsPapers;
        }

        public NewsPaper GetNewpaperById(int newspaperId)
        {
            foreach (NewsPaper newsPaper in newsPapers)
            {
                if (newsPaper.Id == newspaperId)
                {
                    return newsPaper;
                }
            }
            return null;
        }
    }
}
