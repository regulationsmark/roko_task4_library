﻿using System;
using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL;
using Davydova_Task4_Library.BLL.Implementations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL;
<<<<<<< Updated upstream
using Davydova_Task4_Library.DAL_Interfaces;
=======
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Davydova_Task4_Library.RealDAL;
>>>>>>> Stashed changes

namespace Davydova_Task4_Library.PL
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleIO.PrintMenu();
            while (true)
            {
<<<<<<< Updated upstream
                var action = Console.ReadLine();
                switch (action)
                {
                    case "0":
                        ConsoleIO.AddAuthor();
                        break;
                    case "1":
                        ConsoleIO.AddBook();
                        break;
                    case "2":
                        ConsoleIO.AddNewspaper();
                        break;
                    case "3":
                        ConsoleIO.AddPatent();
                        break;
                    case "4":
                        ConsoleIO.DeleteAuthor();
                        break;
                    case "5":
                        ConsoleIO.DeleteBook();
                        break;
                    case "6":
                        ConsoleIO.DeleteNewspaper();
                        break;
                    case "7":
                        ConsoleIO.DeletePatent();
                        break;
                    case "8":
                        ConsoleIO.ShowAllRepository();
                        break;
                    case "9":
                        ConsoleIO.GetLibraryObjectByName();
                        break;
                    case "10":
                        ConsoleIO.SortBookByPublicationYear();
                        ConsoleIO.ShowAllBooks();
                        break;
                    case "11":
                        ConsoleIO.ReverseSortBookByPublicationYear();
                        ConsoleIO.ShowAllBooks();
                        break;
                    case "12":
                        ConsoleIO.GetBooksByAuthor();
                        break;
                    case "13":
                        ConsoleIO.GetPatentsByInventor();
                        break;
                    case "14":
                        ConsoleIO.GetBooksAndPatentsByInventor();
                        break;
                    case "15":
                        ConsoleIO.GetBooksByCharacterSet();
                        break;
                    case "16":
                        ConsoleIO.GroupByPublicationYear();
                        break;
                    default:
                        break;
                }
            }
=======
                services.AddAuthorDao()
                .AddAuthorRealDao()
                .AddBookRealDao()
                .AddBaseRealDao()
                .AddLibraryObjectDao()
                .AddPublicationDao()
                .AddBaseDao()
                .AddBookDao()
                .AddNewspaperDao()
                .AddPatentDao()
                .AddAuthorLogic()
                .AddLibraryObjectLogic()
                .AddPublicationLogic()
                .AddBookLogic()
                .AddNewspaperLogic()
                .AddPatentLogic();
                services.AddHostedService<Worker>();
            });

            host.ConfigureLogging((hostingContext, loggingBuilder) =>
            {
                loggingBuilder.ClearProviders();

                var logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo.File("data.log")
                    .WriteTo.Console()
                    .CreateLogger();

                loggingBuilder.AddSerilog(logger, dispose: true);
            });

            return host;
>>>>>>> Stashed changes
        }
    }
}
