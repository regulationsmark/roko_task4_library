﻿using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.DAL_Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.DAL.Interfaces;

namespace Davydova_Task4_Library.DAL
{
    public class AuthorDao:IAuthorDao
    {
        List<Author> authors = new List<Author>();

        public Author GetAuthorById(int idAuthor)
        {
            foreach (Author author in authors)
            {
                if (author.Id == idAuthor)
                {
                    return author;
                }
            }
            return null;
        }

        public int AddAuthor(Author author)
        {
            authors.Add(author);
            return authors.Last().Id;
        }

        public bool DeleteAuthor(int authorId)
        {
            foreach (Author author in authors)
            {
                if (author.Id == authorId)
                {
                    return authors.Remove(author);
                }
            }
            return false;
        }

        public IEnumerable<Author> GetAllAuthors()
        {
            return authors;
        }

        public Author GetAuthorByName(string nameAuthor)
        {
            foreach (Author author in authors)
            {
                if (author.Name == nameAuthor)
                {
                    return author;
                }
            }
            return null;
        }

        public IEnumerable<Author> GetAuthorsByName(string nameAuthor)
        {
            throw new NotImplementedException();
        }

        public void UpdateAuthor(Author author)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Author> GetAuthorsByBookId(int bookId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Author> GetInventorsByPatentId(int patentId)
        {
            throw new NotImplementedException();
        }
    }
}
