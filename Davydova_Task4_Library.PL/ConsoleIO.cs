﻿using Davydova_Task4_Library.BLL;
using Davydova_Task4_Library.BLL.Implementations;
using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.DAL;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.PL
{
    public class ConsoleIO
    {
        private static IAuthorDao authorDao = new AuthorDao();
        private static IBaseDao baseDao = new BaseDao();
        private static IBookDao bookDao = new BookDao();
        private static ILibraryObject libraryDao = new LibraryDao();
        private static INewspaperDao newspaperDao = new NewspaperDao();
        private static IPatentDao patentDao = new PatentDao();
        private static IPublicationDao publicationDao = new PublicationDao();

        private static IAuthorLogic authorLogic = new AuthorLogic(authorDao);
        private static IBookLogic bookLogic = new BookLogic(bookDao, baseDao, authorDao);
        private static ILibraryObjectLogic libraryObjectLogic = new LibraryObjectLogic(libraryDao);
        private static INewspaperLogic newspaperLogic = new NewsPaperLogic(newspaperDao);
        private static IPatientLogic patientLogic = new PatentLogic(patentDao, baseDao, authorDao);
        private static IPublicationLogic publicationLogic = new PublicationLogic(publicationDao);
        public static void PrintMenu()
        {
            Console.WriteLine("Enter action: ");
            Console.WriteLine("0 - Add author");
            Console.WriteLine("1 - Add book to repository");
            Console.WriteLine("2 - Add newspaper to repository");
            Console.WriteLine("3 - Add patent to repository");
            Console.WriteLine("4 - Delete author");
            Console.WriteLine("5 - Delete book from repository");
            Console.WriteLine("6 - Delete newspaper from repository");
            Console.WriteLine("7 - Delete patent from repository");
            Console.WriteLine("8 - Show all repository");
            Console.WriteLine("9 - Get library object by name");
            Console.WriteLine("10 - Sort book by publication year");
            Console.WriteLine("11 - Reverse sort book by publication year");
            Console.WriteLine("12 - Get all books by author");
            Console.WriteLine("13 - Get all patents by inventor");
            Console.WriteLine("14 - Get all books and patents by author");
            Console.WriteLine("15 - Get all book by character set with group by publication house");
            Console.WriteLine("16 - Group by publication year");
        }

        public static void AddBook()
        {
            Console.WriteLine("Enter id book");
            int id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter name of book");
            string name = Console.ReadLine();

            Console.WriteLine("Enter count of pages");
            string countOfPages = Console.ReadLine();

            Console.WriteLine("Enter note about book");
            string note = Console.ReadLine();

            Console.WriteLine("Enter place of publication");
            string placeOfPublication = Console.ReadLine();

            Console.WriteLine("Enter publishing house");
            string publishingHouse = Console.ReadLine();

            Console.WriteLine("Enter publishing year");
            string publishingYear = Console.ReadLine();

            Console.WriteLine("Enter id of authors");
            string[] authors = Console.ReadLine().Split(' ');
            List<Author> listOfAuthors = new List<Author>();

            for (int i = 0; i < authors.Length; i++)
            {
                listOfAuthors.Add(authorLogic.GetAuthorById(int.Parse(authors[i])));
            }

            Console.WriteLine("Enter ISBN");
            string iSBN = Console.ReadLine();

<<<<<<< Updated upstream
            Console.WriteLine($"Book with id {bookLogic.AddBook(id, name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear, listOfAuthors, iSBN)} was added");            
            int year;
            bool success = Int32.TryParse(publishingYear, out year);
            if (success)
=======
        public static List<string> EnterAuthors()
        {
            Console.WriteLine("Enter count of authors");
            int countOfAuthors = int.Parse(Console.ReadLine());
            string[] authors = new string[countOfAuthors];
            Console.WriteLine("Enter ids authors: ");
            for (int i = 0; i < countOfAuthors; i++)
>>>>>>> Stashed changes
            {
                libraryObjectLogic.AddLibraryObject(id, name, year, countOfPages, note);
            }
        }

        public static void AddAuthor()
        {
            Console.WriteLine("Enter id of author");
            int id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter name of author");
            string name = Console.ReadLine();

            Console.WriteLine("Enter surname of author");
            string surname = Console.ReadLine();

            Console.WriteLine($"Author with id {authorLogic.AddAuthor(id, name, surname)} was added");
        }
        public static void AddNewspaper()
        {
            Console.WriteLine("Enter id book");
            int id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter name of book");
            string name = Console.ReadLine();

            Console.WriteLine("Enter publishing year");
            string publishingYear = Console.ReadLine();

            Console.WriteLine("Enter count of pages");
            string countOfPages = Console.ReadLine();

            Console.WriteLine("Enter note about book");
            string note = Console.ReadLine();

            Console.WriteLine("Enter place of publication");
            string placeOfPublication = Console.ReadLine();

            Console.WriteLine("Enter publishing house");
            string publishingHouse = Console.ReadLine();

            Console.WriteLine("Enter issue number");
            int issueNumber = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter issue date");
            DateTime issueDate = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Enter ISSN");
            string iSSN = Console.ReadLine();

            Console.WriteLine($"Newspaper with id {newspaperLogic.AddNewspaper(id, name, publishingYear, countOfPages, note, placeOfPublication, publishingHouse, issueNumber, issueDate, iSSN)} was added");
            int year;
            bool success = Int32.TryParse(publishingYear, out year);
            if (success)
            {
                libraryObjectLogic.AddLibraryObject(id, name, year, countOfPages, note);
            }
        }

        public static void AddPatent()
        {
            Console.WriteLine("Enter id book");
            int id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter name of book");
            string name = Console.ReadLine();

            Console.WriteLine("Enter count of pages");
            string countOfPages = Console.ReadLine();

            Console.WriteLine("Enter note about book");
            string note = Console.ReadLine();

            Console.WriteLine("Enter place of publication");
            string placeOfPublication = Console.ReadLine();

            Console.WriteLine("Enter id of authors");
            string[] inventors = Console.ReadLine().Split(' ');
            List<Author> listOfInventors = new List<Author>();

            for (int i = 0; i < inventors.Length; i++)
            {
                listOfInventors.Add(authorLogic.GetAuthorById(int.Parse(inventors[i])));
            }

            Console.WriteLine("Enter registration number");
            string registrationNumber = Console.ReadLine();

            Console.WriteLine("Enter application date");
            DateTime applicationDate = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Enter application date");
            DateTime publicationDate = DateTime.Parse(Console.ReadLine());

            Console.WriteLine("Enter country");
            string country = Console.ReadLine();
            
            Console.WriteLine($"Patent with id {patientLogic.AddPatent(id, name, countOfPages, note, placeOfPublication, listOfInventors, registrationNumber, applicationDate, publicationDate, country)} was added");
            libraryObjectLogic.AddLibraryObject(id, name, applicationDate.Year, countOfPages, note);

        }

        public static void DeleteAuthor()
        {
            Console.WriteLine("Enter id author");
            int idAuthor = int.Parse(Console.ReadLine());
            authorLogic.DeleteAuthor(idAuthor);
            Console.WriteLine("Author deleted");
        }

        public static void DeleteBook()
        {
            Console.WriteLine("Enter book id");
            int bookId = int.Parse(Console.ReadLine());
            bookLogic.DeleteBook(bookId);
            libraryObjectLogic.DeleteLibraryObject(bookId);
            Console.WriteLine("Book deleted");
        }

        public static void DeleteNewspaper()
        {
            Console.WriteLine("Enter id newspaper");
            int idNewspaper = int.Parse(Console.ReadLine());
            newspaperLogic.DeleteNewspaper(idNewspaper);
            libraryObjectLogic.DeleteLibraryObject(idNewspaper);
            Console.WriteLine("Newspaper deleted");
        }

        public static void DeletePatent()
        {
            Console.WriteLine("Enter id patent");
            int idPatent = int.Parse(Console.ReadLine());
            patientLogic.DeletePatent(idPatent);
            libraryObjectLogic.DeleteLibraryObject(idPatent);
            Console.WriteLine("Patent deleted");
        }

        public static void ShowAllRepository()
        {
            foreach (LibraryObject libraryObject in libraryObjectLogic.GetAllLibraryObjects())
            {
                Console.WriteLine(libraryObject.ToString());
            }
        }

        public static void GetLibraryObjectByName()
        {
            Console.WriteLine("Enter name");
            string name = Console.ReadLine();
            Console.WriteLine(libraryObjectLogic.GetLibraryObjectByName(name).ToString());
        }

        public static void SortBookByPublicationYear()
        {
            bookLogic.GetSortedBookByPublicationYear();
        }

        public static void ReverseSortBookByPublicationYear()
        {
            bookLogic.GetReverseSortedBookByPublicationYear();
        }

        public static void ShowAllBooks()
        {
            foreach(Book book in bookLogic.GetAllBooks())
            {
                Console.WriteLine(book.ToString());
            }
        }

        public static void GetBooksByAuthor()
        {
            Console.WriteLine("Enter author name");
            string name = Console.ReadLine();
            Console.WriteLine(bookLogic.GetBooksByNameAuthor(name).ToString());
        }

        public static void GetPatentsByInventor()
        {
            Console.WriteLine("Enter inventor name");
            string name = Console.ReadLine();
            Console.WriteLine(patientLogic.GetPatentsByNameInventor(name).ToString());
        }

        public static void GetBooksAndPatentsByInventor()
        {
            Console.WriteLine("Enter author name");
            string name = Console.ReadLine();
            Console.WriteLine(bookLogic.GetBooksByNameAuthor(name).ToString());
            Console.WriteLine(patientLogic.GetPatentsByNameInventor(name).ToString());
        }

        public static void GetBooksByCharacterSet()
        {
            Console.WriteLine("Enter character set as one string");
            string characterSet = Console.ReadLine();
            foreach (Book book in bookLogic.GetBooksByCharacterSet(characterSet))
            {
                Console.WriteLine(book.ToString());
            }
        }

        public static void GroupByPublicationYear()
        {
            foreach(var item1 in bookLogic.GroupByPublicationYear())
            {
                foreach (var item2 in item1)
                {
                    Console.WriteLine(item2.ToString());
                }
            }
            
        }
    }
}
