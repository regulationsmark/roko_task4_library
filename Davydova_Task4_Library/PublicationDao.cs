﻿using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class PublicationDao : IPublicationDao
    {
        List<Publication> publications = new List<Publication>();
        public int AddPublication(Publication publication)
        {
            publications.Add(publication);
            return publications.Last().Id;
        }

        public bool DeletePublication(int publicationId)
        {
            foreach (Publication publication in publications)
            {
                if (publication.Id == publicationId)
                {
                    return publications.Remove(publication);
                }
            }
            return false;
        }

        public IEnumerable<Publication> GetAllPublications()
        {
            return publications;
        }

        public Publication GetPublicationById(int publicationId)
        {
            foreach (Publication publication in publications)
            {
                if (publication.Id == publicationId)
                {
                    return publication;
                }
            }
            return null;
        }
    }
}
