﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL
{
    public class NewsPaperLogic : INewspaperLogic
    {
        private INewspaperDao _newspaperDao;

        public NewsPaperLogic(INewspaperDao newspaperDaoDao)
        {
            _newspaperDao = newspaperDaoDao;
        }
        public int AddNewspaper(int id, string name, string publicationYear, string countOfPages, string note, string placeOfPublication, string publishingHouse, int issueNumber, DateTime issueDate, string iSSN)
        {
            NewspaperValidation newspaperValidation = new NewspaperValidation();
            PublicationValidation publicationValidation = new PublicationValidation();
            LibraryObjectValidation libraryObjectValidation = new LibraryObjectValidation();
            publicationValidation.ExecutePublicationValidation(publishingHouse, publicationYear, placeOfPublication);
            libraryObjectValidation.ExecuteLibraryObjectValidation(name, countOfPages, note);
            if (!publicationValidation.exceptionsList.Any() && !libraryObjectValidation.exceptionsList.Any())
            {
                newspaperValidation.ExecuteNewspaperValidation(iSSN, issueDate, int.Parse(publicationYear));
                if (!newspaperValidation.exceptionsList.Any())
                {
                    foreach (NewsPaper newspaperItem in _newspaperDao.GetAllNewspapers())
                    {
                        if (newspaperItem.ISSN == iSSN)
                        {
<<<<<<< Updated upstream
                            throw new ArgumentException("ISSN must be unique.");
=======
                            //ConsoleIO.PrintInformation($"ISSN must be unique.");
                            _logger.LogInformation($"ISSN must be unique.");//edit checking unique
                            return 0;
>>>>>>> Stashed changes
                        }
                    }
                    NewsPaper newspaper = new NewsPaper(id, name, int.Parse(publicationYear), int.Parse(countOfPages), note, placeOfPublication, publishingHouse, issueNumber, issueDate, iSSN);
                    return _newspaperDao.AddNewsPaper(newspaper);
                }
            }
            else
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in newspaperValidation.exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                throw new ValidationException(failedValidation.ToString());
            }
            return 0;
        }

        public bool DeleteNewspaper(int newspaperId)
        {
            return _newspaperDao.DeleteNewsPaper(newspaperId);
        }

        public IEnumerable<NewsPaper> GetAllNewsPapers()
        {
            return _newspaperDao.GetAllNewspapers();
        }

        public NewsPaper GetNewspaperById(int newspaperId)
        {
            return _newspaperDao.GetNewpaperById(newspaperId);
        }
    }
}
